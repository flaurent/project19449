### A Pluto.jl notebook ###
# v0.19.26

using Markdown
using InteractiveUtils

# ╔═╡ 11ac5f6a-0f6a-11ee-2598-11c7a0064b43
using StatsBase, HypothesisTests, XLSX, SparseArrays, MultipleTesting

# ╔═╡ 01fbf1c9-1cea-4593-a2c5-03854dee2473
md"""
This document is a Pluto notebook (or an html rendered version of it) and requires [Julia](https://computationalthinking.mit.edu/Spring21/installation/).
"""

# ╔═╡ 8dd5d4b9-5a32-4a95-8f38-7f3e5b98e2d9
md"""
We first load the count data.

Note the notebook is assumed to run from a child directory (*e.g.* `notebooks`) of the project root, and the data files are expected to be found in `data/raw`, again from the project root.
"""

# ╔═╡ 5e94a517-db46-4864-b717-5b4543884dff
data = XLSX.openxlsx("../data/raw/SNPspecifiques.xlsx")

# ╔═╡ a5e61a48-e07e-49dc-8c61-1224d1c47d13
counts = Int.(data["Nb SNP"]["B3:U25"])

# ╔═╡ a25eecd6-f58d-493d-a5cf-94218319849d
md"""
For further reference, we also load the labels of the rows (`categories`) and columns (`genotypes`).
"""

# ╔═╡ 9c3b8eb2-a160-4246-a5c7-596a86c73d62
begin
	genotypes = vec(data["Nb SNP"]["B2:U2"]) # columns
	categories = vec(data["Nb SNP"]["A3:A25"]) # rows
end

# ╔═╡ 3eb9d39c-2ca1-430e-b13b-05c809a80d5b
md"""
First, we exclude the rows and columns of zeros only.
"""

# ╔═╡ e3febec7-b84c-45f9-8f65-5a158b0ae933
begin
	row_ok = vec(any(!=(0), counts; dims=2))
	col_ok = vec(any(!=(0), counts; dims=1))
	clean_counts = counts[row_ok, col_ok]
	nrows, ncols = size(clean_counts)
end

# ╔═╡ f2825d3c-577b-4b61-8c8c-c869a9ec9f07
md"""
To test whether a genotype exhibits a different partition into categories of genes, or equivalently whether it is enriched in some category, we first derive an expected partition averaging the category frequencies across all genotypes.
"""

# ╔═╡ 72cf8d55-a423-4c28-a370-377f9ef6a70e
begin
	col_total = sum(clean_counts; dims=1)
	expected_frequencies = mean(clean_counts ./ col_total; dims=2)
	@assert sum(expected_frequencies) ≈ 1
	# print the frequencies with the corresponding category names
	Dict(zip(categories[row_ok], expected_frequencies))
end

# ╔═╡ b8adc566-5893-402a-a35d-11c06460fb62
md"""
Due to the presence of zeros in most columns, we have to group the low-frequency cells into single cells on a per-column basis. The information in low-frequency cells is already accounted for by the other cells.

We can also individually test each count `observed_n` _equal to or greater than 5_ in a contingency table with:
```
+------------+------------------------+
| observed_n | col_total - observed_n |
+------------+------------------------+
| expected_n | col_total - expected_n |
+------------+------------------------+
```
This second approach is less powerful, though.

Let us begin with this second approach:
"""

# ╔═╡ 3df39484-e07a-48d8-90ae-09da0050b3ce
begin
	observed_counts = clean_counts
	# in theory, we could also use real-valued counts instead
	expected_counts = @. round(Int, expected_frequencies * col_total)
	observed_remainder = col_total .- observed_counts
	expected_remainder = col_total .- expected_counts

	pvalues = spzeros(nrows, ncols)
	for row in 1:nrows
		for col in 1:ncols
			
			# build the contingency table
			a = observed_counts[row, col]
			b = observed_remainder[row, col]
			c = expected_counts[row, col]
			d = expected_remainder[row, col]
			contingency_table = [ a b ; c d ]
			
			# perform a χ² test of homogeneity
			if all(>=(5), contingency_table)
				pval = pvalue(ChisqTest(contingency_table))
				# note: FisherExactTest(a, b, c, d) gives very similar results
				#pval = pvalue(FisherExactTest(a, b, c, d); method=:minlike)
				
				pvalues[row, col] = pval
			end
		end
	end
	pvalues
end

# ╔═╡ b314add7-a6f5-42f0-bc9a-bba748543fa2
md"""
Out of curiosity, with no correction for multiple comparisons, we find a single p-value below 0.05:
"""

# ╔═╡ 7e383967-26de-49bd-a106-d215241ffdb1
begin
	uncorrected_pvalues_below_threshold = copy(pvalues)
	uncorrected_pvalues_below_threshold.nzval[.05 .< pvalues.nzval] .= 0
	dropzeros(uncorrected_pvalues_below_threshold)
end

# ╔═╡ c09b771c-aab1-40cf-a273-14aa6c857510
md"""
Correcting for all the comparisons, no differences survive:
"""

# ╔═╡ 46c9be07-3bb4-499c-a819-7a87c73b6e42
begin
	# see https://github.com/juliangehring/MultipleTesting.jl#two-step
	corrected_pvalues = adjust(pvalues.nzval, BenjaminiHochbergAdaptive(TwoStep()))
	
	significant = corrected_pvalues .<= .05
	significant_pvalues = copy(pvalues)
	significant_pvalues.nzval .= 0
	significant_pvalues.nzval[significant] = corrected_pvalues[significant]
	significant_pvalues = dropzeros(significant_pvalues)
end

# ╔═╡ 128668c3-282d-4453-84a6-cf2c29d7140f
md"""
Had we found differences, we could get the corresponding category-genotype pairs as follows:
"""

# ╔═╡ 548ca244-de4c-47c2-a69e-0ca7a449028f
begin
	row, col = findnz(significant_pvalues)
	category = categories[row_ok][row]
	genotype = genotypes[col_ok][col]
	collect(zip(category, genotype))
end

# ╔═╡ 884a7299-8418-45e4-82f3-65204d48e55b
md"""
The sample size is too low if we consider the large number of cells, and we need a more powerful test.
Let us now compare each genotype with the "average" genotype, grouping the low-frequency cells together.

We already know the post-hoc tests fail, therefore we won't have category-level resolution. However, a significant difference is still possible here as the test will cumulate the differences for the different categories. The individual differences should be detectable increasing the sample size.

# Disclaimer

The following analysis is a second iteration motivated by an error in the first iteration. The history of the present notebook can be found [here](https://gitlab.pasteur.fr/flaurent/project19449/-/blob/main/notebooks/snp_chisq_tests.jl).

The categories were grouped based on the observed counts, whereas the minimum-5 criterion actually applies to the expected counts. This is fixed in this version of the analysis.

# Grouping procedure

An elaborate grouping approach can be found in [Kass, 1980](https://www4.stat.ncsu.edu/~dickey/analytics/datamine/Reference%20Papers/kass80.pdf).

The simpler approach followed here consists in grouping all the categories with an expected count below 5 into a single synthetic category, for each column separately. Observed counts are similarly grouped, based on the expected counts.
"""

# ╔═╡ 9342e489-a297-4c67-8324-cb41801af713
begin
	per_genotype_pvalues = Dict{String, Float64}()
	for col in 1:ncols
		# identify cells to be grouped
		ok = expected_counts[:, col] .>= 5
		any(ok) || continue
		# group cells
		expected_counts′ = expected_counts[ok, col]
		if !all(ok)
			synthetic_count = sum(expected_counts[(!).(ok), col])
			push!(expected_counts′, synthetic_count)
		end
		observed_counts′ = observed_counts[ok, col]
		if !all(ok)
			synthetic_count = sum(observed_counts[(!).(ok), col])
			push!(observed_counts′, synthetic_count)
		end
		# perform the test
		contingency_table = hcat(observed_counts′, expected_counts′)
		pval = pvalue(ChisqTest(contingency_table))
		# record the resulting pvalue
		genotype = genotypes[col_ok][col]
		per_genotype_pvalues[genotype] = pval
	end
	per_genotype_pvalues
end

# ╔═╡ 6d768ec2-f9ad-4cc7-a2df-4eeb1a72cd44
md"""
Not any uncorrected p-value drops below 0.05.
"""

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
HypothesisTests = "09f84164-cd44-5f33-b23f-e6b0d136a0d5"
MultipleTesting = "f8716d33-7c4a-5097-896f-ce0ecbd3ef6b"
SparseArrays = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"
StatsBase = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
XLSX = "fdbf4ff8-1666-58a4-91e7-1b58723a45e0"

[compat]
HypothesisTests = "~0.11.0"
MultipleTesting = "~0.5.1"
StatsBase = "~0.33.21"
XLSX = "~0.9.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.9.0"
manifest_format = "2.0"
project_hash = "d9da7e97863fee97b88704272749d1c852674499"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"
version = "1.1.1"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Calculus]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f641eb0a4f00c343bbc32346e1217b86f3ce9dad"
uuid = "49dc2e85-a5d0-5ad3-a950-438e2897f1b9"
version = "0.5.1"

[[deps.ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "e30f2f4e20f7f186dc36529910beaedc60cfa644"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "1.16.0"

[[deps.Combinatorics]]
git-tree-sha1 = "08c8b6831dc00bfea825826be0bc8336fc369860"
uuid = "861a8166-3701-5b0c-9a16-15d98fcdc6aa"
version = "1.0.2"

[[deps.CommonSolve]]
git-tree-sha1 = "0eee5eb66b1cf62cd6ad1b460238e60e4b09400c"
uuid = "38540f10-b2f7-11e9-35d8-d573e4eb0ff2"
version = "0.2.4"

[[deps.Compat]]
deps = ["UUIDs"]
git-tree-sha1 = "7a60c856b9fa189eb34f5f8a6f6b5529b7942957"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "4.6.1"
weakdeps = ["Dates", "LinearAlgebra"]

    [deps.Compat.extensions]
    CompatLinearAlgebraExt = "LinearAlgebra"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "1.0.2+0"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "738fec4d684a9a6ee9598a8bfee305b26831f28c"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.5.2"

    [deps.ConstructionBase.extensions]
    ConstructionBaseIntervalSetsExt = "IntervalSets"
    ConstructionBaseStaticArraysExt = "StaticArrays"

    [deps.ConstructionBase.weakdeps]
    IntervalSets = "8197267c-284f-5f27-9208-e0e47529a953"
    StaticArrays = "90137ffa-7385-5640-81b9-e52037218182"

[[deps.DataAPI]]
git-tree-sha1 = "8da84edb865b0b5b0100c0666a9bc9a0b71c553c"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.15.0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "d1fff3a548102f48987a52a2e0d114fa97d730f0"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.13"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Distributions]]
deps = ["FillArrays", "LinearAlgebra", "PDMats", "Printf", "QuadGK", "Random", "SparseArrays", "SpecialFunctions", "Statistics", "StatsAPI", "StatsBase", "StatsFuns", "Test"]
git-tree-sha1 = "4ed4a6df2548a72f66e03f3a285cd1f3b573035d"
uuid = "31c24e10-a181-5473-b8eb-7969acd0382f"
version = "0.25.96"

    [deps.Distributions.extensions]
    DistributionsChainRulesCoreExt = "ChainRulesCore"
    DistributionsDensityInterfaceExt = "DensityInterface"

    [deps.Distributions.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
    DensityInterface = "b429d917-457f-4dbc-8f4c-0cc954292b1d"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "2fb1e02f2b635d0845df5d7c167fec4dd739b00d"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.9.3"

[[deps.Downloads]]
deps = ["ArgTools", "FileWatching", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"
version = "1.6.0"

[[deps.DualNumbers]]
deps = ["Calculus", "NaNMath", "SpecialFunctions"]
git-tree-sha1 = "5837a837389fccf076445fce071c8ddaea35a566"
uuid = "fa6b7ba4-c1ee-5f82-b5fc-ecf0adba8f74"
version = "0.6.8"

[[deps.EzXML]]
deps = ["Printf", "XML2_jll"]
git-tree-sha1 = "0fa3b52a04a4e210aeb1626def9c90df3ae65268"
uuid = "8f5d6c58-4d21-5cfd-889c-e3ad7ee6a615"
version = "1.1.0"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FillArrays]]
deps = ["LinearAlgebra", "Random", "SparseArrays", "Statistics"]
git-tree-sha1 = "e17cc4dc2d0b0b568e80d937de8ed8341822de67"
uuid = "1a297f60-69ca-5386-bcde-b61e274b549b"
version = "1.2.0"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.HypergeometricFunctions]]
deps = ["DualNumbers", "LinearAlgebra", "OpenLibm_jll", "SpecialFunctions"]
git-tree-sha1 = "0ec02c648befc2f94156eaef13b0f38106212f3f"
uuid = "34004b35-14d8-5ef3-9330-4cdb6864b03a"
version = "0.3.17"

[[deps.HypothesisTests]]
deps = ["Combinatorics", "Distributions", "LinearAlgebra", "Printf", "Random", "Rmath", "Roots", "Statistics", "StatsAPI", "StatsBase"]
git-tree-sha1 = "4b5d5ba51f5f473737ed9de6d8a7aa190ad8c72f"
uuid = "09f84164-cd44-5f33-b23f-e6b0d136a0d5"
version = "0.11.0"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IrrationalConstants]]
git-tree-sha1 = "630b497eafcc20001bba38a4651b327dcfc491d2"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.2.2"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"
version = "0.6.3"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"
version = "7.84.0+0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"
version = "1.10.2+0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.Libiconv_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "c7cb1f5d892775ba13767a87c7ada0b980ea0a71"
uuid = "94ce4f54-9a6c-5748-9c1c-f9c7231a4531"
version = "1.16.1+2"

[[deps.LinearAlgebra]]
deps = ["Libdl", "OpenBLAS_jll", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["DocStringExtensions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "c3ce8e7420b3a6e071e0fe4745f5d4300e37b13f"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.24"

    [deps.LogExpFunctions.extensions]
    LogExpFunctionsChainRulesCoreExt = "ChainRulesCore"
    LogExpFunctionsChangesOfVariablesExt = "ChangesOfVariables"
    LogExpFunctionsInverseFunctionsExt = "InverseFunctions"

    [deps.LogExpFunctions.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
    ChangesOfVariables = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
    InverseFunctions = "3587e190-3f89-42d0-90ee-14403ec27112"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "42324d08725e200c23d4dfb549e0d5d89dede2d2"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.10"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"
version = "2.28.2+0"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "f66bdc5de519e8f8ae43bdc598782d35a25b1272"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.1.0"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"
version = "2022.10.11"

[[deps.MultipleTesting]]
deps = ["Distributions", "SpecialFunctions", "StatsBase"]
git-tree-sha1 = "94df14d821baa34662a1492b10615125cbebd47b"
uuid = "f8716d33-7c4a-5097-896f-ce0ecbd3ef6b"
version = "0.5.1"

[[deps.NaNMath]]
deps = ["OpenLibm_jll"]
git-tree-sha1 = "0877504529a3e5c3343c6f8b4c0381e57e4387e4"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "1.0.2"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"
version = "1.2.0"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"
version = "0.3.21+4"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"
version = "0.8.1+0"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "d321bf2de576bf25ec4d3e4360faca399afca282"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.6.0"

[[deps.PDMats]]
deps = ["LinearAlgebra", "SparseArrays", "SuiteSparse"]
git-tree-sha1 = "67eae2738d63117a196f497d7db789821bce61d1"
uuid = "90014a1f-27ba-587c-ab20-58faa44d9150"
version = "0.11.17"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "FileWatching", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
version = "1.9.0"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "7eb1686b4f04b82f96ed7a4ea5890a4f0c7a09f1"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.4.0"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.QuadGK]]
deps = ["DataStructures", "LinearAlgebra"]
git-tree-sha1 = "6ec7ac8412e83d57e313393220879ede1740f9ee"
uuid = "1fd47b50-473d-5c70-9696-f719f8f3bcdc"
version = "2.8.2"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Rmath]]
deps = ["Random", "Rmath_jll"]
git-tree-sha1 = "f65dcb5fa46aee0cf9ed6274ccbd597adc49aa7b"
uuid = "79098fc4-a85e-5d69-aa6a-4863f24498fa"
version = "0.7.1"

[[deps.Rmath_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "6ed52fdd3382cf21947b15e8870ac0ddbff736da"
uuid = "f50d1b31-88e8-58de-be2c-1cc44531875f"
version = "0.4.0+0"

[[deps.Roots]]
deps = ["ChainRulesCore", "CommonSolve", "Printf", "Setfield"]
git-tree-sha1 = "de432823e8aab4dd1a985be4be768f95acf152d4"
uuid = "f2b01f46-fcfa-551c-844a-d8ac1e96c665"
version = "2.0.17"

    [deps.Roots.extensions]
    RootsForwardDiffExt = "ForwardDiff"
    RootsIntervalRootFindingExt = "IntervalRootFinding"
    RootsSymPyExt = "SymPy"

    [deps.Roots.weakdeps]
    ForwardDiff = "f6369f11-7733-5829-9624-2563aa707210"
    IntervalRootFinding = "d2bf35a9-74e0-55ec-b149-d360ff49b807"
    SymPy = "24249f21-da20-56a4-8eb1-6a02cf4ae2e6"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"
version = "0.7.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Setfield]]
deps = ["ConstructionBase", "Future", "MacroTools", "StaticArraysCore"]
git-tree-sha1 = "e2cc6d8c88613c05e1defb55170bf5ff211fbeac"
uuid = "efcf1570-3423-57d1-acb7-fd33fddbac46"
version = "1.1.1"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "c60ec5c62180f27efea3ba2908480f8055e17cee"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.1.1"

[[deps.SparseArrays]]
deps = ["Libdl", "LinearAlgebra", "Random", "Serialization", "SuiteSparse_jll"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SpecialFunctions]]
deps = ["IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "ef28127915f4229c971eb43f3fc075dd3fe91880"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.2.0"
weakdeps = ["ChainRulesCore"]

    [deps.SpecialFunctions.extensions]
    SpecialFunctionsChainRulesCoreExt = "ChainRulesCore"

[[deps.StaticArraysCore]]
git-tree-sha1 = "6b7ba252635a5eff6a0b0664a41ee140a1c9e72a"
uuid = "1e83bf80-4336-4d27-bf5d-d5a4f845583c"
version = "1.4.0"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
version = "1.9.0"

[[deps.StatsAPI]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "45a7769a04a3cf80da1c1c7c60caf932e6f4c9f7"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.6.0"

[[deps.StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "LogExpFunctions", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "d1bf48bfcc554a3761a133fe3a9bb01488e06916"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.33.21"

[[deps.StatsFuns]]
deps = ["HypergeometricFunctions", "IrrationalConstants", "LogExpFunctions", "Reexport", "Rmath", "SpecialFunctions"]
git-tree-sha1 = "f625d686d5a88bcd2b15cd81f18f98186fdc0c9a"
uuid = "4c63d2b9-4356-54db-8cca-17b64c39e42c"
version = "1.3.0"

    [deps.StatsFuns.extensions]
    StatsFunsChainRulesCoreExt = "ChainRulesCore"
    StatsFunsInverseFunctionsExt = "InverseFunctions"

    [deps.StatsFuns.weakdeps]
    ChainRulesCore = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
    InverseFunctions = "3587e190-3f89-42d0-90ee-14403ec27112"

[[deps.SuiteSparse]]
deps = ["Libdl", "LinearAlgebra", "Serialization", "SparseArrays"]
uuid = "4607b0f0-06f3-5cda-b6b1-a6196a1729e9"

[[deps.SuiteSparse_jll]]
deps = ["Artifacts", "Libdl", "Pkg", "libblastrampoline_jll"]
uuid = "bea87d4a-7f5b-5778-9afe-8cc45184846c"
version = "5.10.1+6"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "1544b926975372da01227b382066ab70e574a3ec"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.10.1"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"
version = "1.10.0"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.XLSX]]
deps = ["Artifacts", "Dates", "EzXML", "Printf", "Tables", "ZipFile"]
git-tree-sha1 = "d6af50e2e15d32aff416b7e219885976dc3d870f"
uuid = "fdbf4ff8-1666-58a4-91e7-1b58723a45e0"
version = "0.9.0"

[[deps.XML2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libiconv_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "93c41695bc1c08c46c5899f4fe06d6ead504bb73"
uuid = "02c8fc9c-b97f-50b9-bbe4-9be30ff0a78a"
version = "2.10.3+0"

[[deps.ZipFile]]
deps = ["Libdl", "Printf", "Zlib_jll"]
git-tree-sha1 = "f492b7fe1698e623024e873244f10d89c95c340a"
uuid = "a5390f91-8eb1-5f08-bee0-b1d1ffed6cea"
version = "0.10.1"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.13+0"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
version = "5.7.0+0"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"
version = "1.48.0+0"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
version = "17.4.0+0"
"""

# ╔═╡ Cell order:
# ╟─01fbf1c9-1cea-4593-a2c5-03854dee2473
# ╠═11ac5f6a-0f6a-11ee-2598-11c7a0064b43
# ╟─8dd5d4b9-5a32-4a95-8f38-7f3e5b98e2d9
# ╠═5e94a517-db46-4864-b717-5b4543884dff
# ╠═a5e61a48-e07e-49dc-8c61-1224d1c47d13
# ╟─a25eecd6-f58d-493d-a5cf-94218319849d
# ╠═9c3b8eb2-a160-4246-a5c7-596a86c73d62
# ╟─3eb9d39c-2ca1-430e-b13b-05c809a80d5b
# ╠═e3febec7-b84c-45f9-8f65-5a158b0ae933
# ╟─f2825d3c-577b-4b61-8c8c-c869a9ec9f07
# ╠═72cf8d55-a423-4c28-a370-377f9ef6a70e
# ╟─b8adc566-5893-402a-a35d-11c06460fb62
# ╠═3df39484-e07a-48d8-90ae-09da0050b3ce
# ╠═b314add7-a6f5-42f0-bc9a-bba748543fa2
# ╠═7e383967-26de-49bd-a106-d215241ffdb1
# ╟─c09b771c-aab1-40cf-a273-14aa6c857510
# ╠═46c9be07-3bb4-499c-a819-7a87c73b6e42
# ╟─128668c3-282d-4453-84a6-cf2c29d7140f
# ╠═548ca244-de4c-47c2-a69e-0ca7a449028f
# ╟─884a7299-8418-45e4-82f3-65204d48e55b
# ╠═9342e489-a297-4c67-8324-cb41801af713
# ╟─6d768ec2-f9ad-4cc7-a2df-4eeb1a72cd44
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
