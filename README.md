This repository is related to project 19449 with the [Bioinformatics & Biostatistics Hub](https://research.pasteur.fr/fr/team/bioinformatics-and-biostatistics-hub/) and [Cyril Savin](https://research.pasteur.fr/fr/member/cyril-savin/).

Olivier Mirabeau also contributed to this project, via another channel.

Two notebooks are available, with html exports:

* [snp_chisq_tests](https://flaurent.pages.pasteur.fr/project19449/snp_chisq_tests.html)
* [genes_chisq_tests](https://flaurent.pages.pasteur.fr/project19449/genes_chisq_tests.html)
